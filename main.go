package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/go-resty/resty/v2"
)

type CodeFirstContainer struct {
	ID      string   `json:"Id"`
	Image   string   `json:"Image"`
	Env     []string `json:"Env,omitempty"`
	Admins  string   `json:"Admins,omitempty"`
	Private bool     `json:"Private,omitempty"`
}

type StringSliceFlag struct {
	value []string
}

func (s *StringSliceFlag) String() string {
	return fmt.Sprintf("%s", *s)
}

func (s *StringSliceFlag) Set(v string) error {
	s.value = append(s.value, v)
	return nil
}

var (
	authUser string

	command                           string
	proxyScheme, proxyHost, proxyPath string
	imageName, containerName, admins  string

	private, overwrite, devel bool

	env StringSliceFlag
)

func main() {

	env = StringSliceFlag{}

	flag.StringVar(&command, "command", "list", "list|logs|create|delete|start")
	flag.BoolVar(&devel, "devel", false, "use fake x-forwarded-user")

	flag.StringVar(&proxyScheme, "proxyscheme", "http", "proxy scheme")
	flag.StringVar(&proxyHost, "proxyhost", "dockerproxy:8080", "proxy host")
	flag.StringVar(&proxyPath, "proxypath", "/", "proxy path")

	flag.StringVar(&imageName, "imagename", "", "image name")
	flag.StringVar(&containerName, "containername", "", "container name")
	flag.StringVar(&admins, "admins", "", "admins (comma separated list)")
	flag.BoolVar(&private, "private", false, "private container")
	flag.BoolVar(&overwrite, "overwrite", false, "overwrite existing container")
	flag.Var(&env, "env", "environment variables (separated by spaces)")

	flag.Parse()

	fmt.Println("flags:")
	fmt.Printf("-imagename: %s\n", imageName)
	fmt.Printf("-containername: %s\n", containerName)
	fmt.Printf("-private: %t\n", private)
	fmt.Printf("-admins: %s\n", admins)
	fmt.Printf("-overwrite: %t\n", overwrite)
	fmt.Printf("-env: %s\n", env)

	if command != "list" && containerName == "" {
		fmt.Println("Missing containername parameter.")
		os.Exit(1)
	}

	if command == "create" && imageName == "" {
		fmt.Println("Missing imagename parameter.")
		os.Exit(1)
	}

	if devel {
		authUser = "celeste.barbosa"
	} else {
		authUser = os.Getenv("DRONE_REPO_OWNER")
	}

	containerName = authUser + "-" + containerName
	containerName = strings.ReplaceAll(containerName, ".", "")

	fmt.Printf("authUser: %s\n", authUser)
	fmt.Printf("new containerName: %s\n", containerName)

	if len(authUser) == 0 {
		fmt.Println("Not authenticated.")
		os.Exit(1)
	}

	switch command {
	case "list":
		list()
	case "logs":
		logs()
	case "create":
		if overwrite {
			delete(true)
		}

		if !exist() {
			createImage()
			create()
			start()
		}
	case "start":
		start()
	case "delete":
		delete(false)
	}

}

func exist() bool {
	client := resty.New()

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		Get(fmt.Sprintf("%s://%s/containers/%s/json", proxyScheme, proxyHost, containerName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	return resp.IsSuccess()
}

func list() {
	client := resty.New()

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		Get(fmt.Sprintf("%s://%s/containers/json", proxyScheme, proxyHost))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if resp.IsError() {
		os.Exit(1)
	}
}

func logs() {
	client := resty.New()

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		Get(fmt.Sprintf("%s://%s/containers/%s/logs", proxyScheme, proxyHost, containerName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if resp.IsError() {
		os.Exit(1)
	}
}

func start() {
	client := resty.New()

	container := CodeFirstContainer{
		Image:   imageName,
		Env:     env.value,
		Private: private,
	}

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		SetBody(container).
		Post(fmt.Sprintf("%s://%s/containers/%s/start", proxyScheme, proxyHost, containerName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if resp.IsError() {
		os.Exit(1)
	}
}

func create() {
	client := resty.New()

	container := CodeFirstContainer{
		Image:   imageName,
		Env:     env.value,
		Private: private,
		Admins:  admins,
	}

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		SetBody(container).
		Post(fmt.Sprintf("%s://%s/containers/create/%s", proxyScheme, proxyHost, containerName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if resp.IsError() {
		os.Exit(1)
	}
}

func delete(bypassError bool) {
	client := resty.New()

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		Delete(fmt.Sprintf("%s://%s/containers/%s", proxyScheme, proxyHost, containerName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if !bypassError && resp.IsError() {
		os.Exit(1)
	}
}

func createImage() {
	client := resty.New()

	resp, err := client.R().
		SetHeader("x-forwarded-user", authUser).
		Post(fmt.Sprintf("%s://%s/images/create?fromImage=%s", proxyScheme, proxyHost, imageName))

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(string(resp.Body()))

	if resp.IsError() {
		os.Exit(1)
	}
}
