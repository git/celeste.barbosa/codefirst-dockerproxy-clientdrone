FROM golang:1.19-bullseye
LABEL author="Céleste Barbosa"

# Copying sources.
WORKDIR /go/src/codefirst.iut.uca.fr/git/celeste.barbosa/codefirst-dockerproxy-clientdrone/
COPY . .

# Installing.
RUN go install -v ./...
RUN chmod +x /go/bin/codefirst-dockerproxy-clientdrone

# Copying entrypoint.
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

USER root
EXPOSE 8081
ENTRYPOINT [ "/entrypoint.sh" ]