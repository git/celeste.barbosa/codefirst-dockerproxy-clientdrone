sur le proxy:
```
docker network create cicd_net
./zig-out/bin/codefirst-dockerproxy -d
```

sur le client web:
```
# Ajuster la constante proxyUrl et proxyPath puis compiler.
docker run --rm -it --volume $(pwd):/app sandrokeil/typescript tsc /app/src/index.ts
mv src/index.js build/src/index.js

# Build l'image.
docker build -t hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientweb .

# Run le container.
docker run -p 8081:80 hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-dockerproxy-clientweb
```

-> http://localhost:8081/dockerrunner/

sur le client cd:
```
go run . -proxyhost localhost:8080 -devel -command create -imagename nginx -containername nginx1 -admins john.doe,mickey.mouse
go run . -proxyhost localhost:8080 -devel -command create -imagename nginx -containername nginx2
go run . -proxyhost localhost:8080 -devel -command create -imagename nginx -containername nginx3
```